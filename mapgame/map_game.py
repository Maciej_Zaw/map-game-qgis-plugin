# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapGame
                                 A QGIS plugin
 This plugin allows for running a game involving automatic creation of a map
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2018-11-29
        git sha              : $Format:%H$
        copyright            : (C) 2018 by Maciej Zawlocki
        email                : maciejzawlocki1993@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, Qt
from PyQt5.QtGui import QIcon, QPalette
from PyQt5.QtWidgets import QAction, QComboBox, QLabel, QDialog, QPushButton, QWidget, QHBoxLayout, QVBoxLayout, QSpacerItem

from qgis.core import QgsProject

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .map_game_dialog import MapGameDialog
import os.path

from .src.main_game_dialog import MainGameDialog
from .src.configuration_dialog import ConfigurationDialog


class MapGame:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'MapGame_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = MapGameDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Map Game')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'MapGame')
        self.toolbar.setObjectName(u'MapGame')
        self.games = []

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('MapGame', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/map_game/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Map Game'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Map Game'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar
 
    def run(self):      
        palette = QPalette()
        palette.setColor(QPalette.Background, Qt.white)
        self.dlg.setAutoFillBackground(True)
        self.dlg.setPalette(palette)
        self._layout = None
        self._layout = QVBoxLayout(self.dlg)
        
        playButton = QPushButton()
        playButton.setText("Play")
        playButton.clicked.connect(self.runGame)
        self._layout.addWidget(playButton)
        
        configurationButton = QPushButton()
        configurationButton.setText("Configuration")
        configurationButton.clicked.connect(self.runConfiguration)
        self._layout.addWidget(configurationButton)
        
        self._layout.addSpacing(100)
        
        exitButton = QPushButton()
        exitButton.setText("Exit")
        exitButton.clicked.connect(self.dlg.accept)
        self._layout.addWidget(exitButton)
     
        self.dlg.setLayout(self._layout)
        self.dlg.setWindowFlag(Qt.WindowCloseButtonHint, False);

        self.dlg.show()
        
        result = self.dlg.exec_()
            
    def runConfiguration(self):
        layers = [layer for layer in QgsProject.instance().mapLayers().values()]
        layerNames = [layer.name() for layer in layers]
        result = DropdownSelectionDialog.selectOption(self, "Select layer:", layerNames, layers)
        
        if result is not None:
            configDialog = ConfigurationDialog(None, result)
            configDialog.show()
            configDialog.exec_()
            game = configDialog.getGameData()
            if game is not None:
                self.games.append(game)
    
    def runGame(self):
        gameNames = [game.getName() for game in self.games]
        result = DropdownSelectionDialog.selectOption(self, "Select game:", gameNames, self.games)
        
        if result is not None:
            gameDialog = MainGameDialog(result)
            gameDialog.show()
            gameDialog.exec_()
    
    
class DropdownSelectionDialog(QDialog):
    
    def __init__(self, parent, text, optionNames, optionValues):
        QDialog.__init__(self, parent=parent)
        self._text = text
        self._names = optionNames
        self._values = optionValues
        self._currentIndex = 0
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(" ")
        self.resize(300,150)
        self._layout = QVBoxLayout(self)
        
        self._label = QLabel(self)
        self._label.setAlignment(Qt.AlignCenter);
        self._label.setText(self._text)
        self._layout.addWidget(self._label)
        
        self._combobox = QComboBox(self)     
        for i in range(len(self._names)):
            self._combobox.addItem(self._names[i], self._values[i])
        self._combobox.currentIndexChanged.connect(self._setNewCurrentIndex)
        self._layout.addWidget(self._combobox)
        
        self._controlLayout = QHBoxLayout(self)
        self._controlLayout.setContentsMargins(2, 2, 2, 2)
        
        self._cancelButton = QPushButton(self)
        self._cancelButton.setText("Cancel")
        self._cancelButton.clicked.connect(self.reject)      
        self._controlLayout.addWidget(self._cancelButton)
        
        self._acceptButton = QPushButton(self)
        self._acceptButton.setText("Accept")
        self._acceptButton.clicked.connect(self.accept)      
        self._controlLayout.addWidget(self._acceptButton)
        
        self._controlWidget = QWidget(self)
        self._controlWidget.setLayout(self._controlLayout)
        
        self._layout.addWidget(self._controlWidget)
        self.setLayout(self._layout)
        
        self.setWindowFlag(Qt.WindowCloseButtonHint, False);
    
    def _setNewCurrentIndex(self, index):
        self._currentIndex = index
    
    def getSelectedValue(self):
        return self._combobox.itemData(self._currentIndex)
    
    @staticmethod
    def selectOption(self, text, optionNames, optionValues):
        dialog = DropdownSelectionDialog(None, text, optionNames, optionValues)
        result = dialog.exec_()
        if result == 1:
            return dialog.getSelectedValue()
        else:
            return None