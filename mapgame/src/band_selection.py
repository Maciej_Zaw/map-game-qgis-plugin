from PyQt5.QtWidgets import QLabel, QWidget, QHBoxLayout, QVBoxLayout, QComboBox

class BandSelectionWidget(QWidget):

    def __init__(self, parent, name, bandDataArray, layer, colorId):
        """
            colorId: Id for the color of output band, 0 for red, 1 for green, 2 for blue
        """
        QWidget.__init__(self, parent=parent)
        self._bandData = bandDataArray
        self._name = name
        self._parent = parent
        self._layer = layer
        self._colorId = colorId
        self.initUI()
    
    def initUI(self):
        self._layout = QHBoxLayout()
        self._label = QLabel(self)
        self._label.setText(self._name)
        
        self._combobox = QComboBox(self)
        
        bandNames = []
        for data in self._bandData:
            self._combobox.addItem(data.getName(), data)
        
        self._combobox.currentIndexChanged.connect(self._onChanged)
        
        self._layout.setContentsMargins(2, 2, 2, 2)
        self._layout.addWidget(self._label)
        self._layout.addWidget(self._combobox)
        self.setLayout(self._layout)
        self.show()
    
    def _onChanged(self):
        newIndex = self._combobox.currentIndex()
        renderer = self._layer.renderer()     
        bandNumber = self._bandData[newIndex].getBandNumber()
        if self._colorId == 0:
            renderer.setRedBand(bandNumber)
        elif self._colorId == 1:
            renderer.setGreenBand(bandNumber)
        elif self._colorId == 2:
            renderer.setBlueBand(bandNumber)
        else:
            return
        self._layer.dataProvider().reloadData()
        self._layer.triggerRepaint()
    
    def setIndex(self, index):
        self._combobox.setCurrentIndex(index)
  
  
class BandSelectionMenuWidget(QWidget):
    def __init__(self, parent, bandDataArray, layer):
        QWidget.__init__(self, parent=parent)
        self._parent = parent
        self._bandData = bandDataArray
        self._layer = layer
        self.initUI()
    
    def initUI(self):
        self._layout = QVBoxLayout()
    
        self._redSelection = BandSelectionWidget(self, "Red", self._bandData, self._layer, 0)
        self._redSelection.setIndex(0)
        self._layout.addWidget(self._redSelection)
        self._greenSelection = BandSelectionWidget(self , "Green", self._bandData, self._layer, 1)
        self._greenSelection.setIndex(1)
        self._layout.addWidget(self._greenSelection)
        self._blueSelection = BandSelectionWidget(self, "Blue", self._bandData, self._layer, 2)
        self._blueSelection.setIndex(2)
        self._layout.addWidget(self._blueSelection)
        
        self.setLayout(self._layout)
        self.show()