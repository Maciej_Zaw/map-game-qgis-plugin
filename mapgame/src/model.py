class AreaData:

    def __init__(self, name, color, points = []):
        self._name = name
        self._color = color
        self._points = points
    
    def getColor(self):
        return self._color
    
    def getName(self):
        return self._name
    
    def getPoints(self):
        return self._points
    
    def isConflicting(self, area):
        if self._name == area.getName():
            return True
        color = area.getColor()
        if (self._color.red() == color.red()
            and self._color.green() == color.green()
            and self._color.blue() == color.blue()):
            return True
        return False

class BandData:

    def __init__(self, bandNumber, name):
        self._name = name
        self._bandNumber = bandNumber
    
    def getBandNumber(self):
        return self._bandNumber
    
    def getName(self):
        return self._name

        
class GameData:

    def __init__(self, name, layer, areaData, bandData):
        self._name = name
        self._layer = layer
        self._areaData = areaData
        self._bandData = bandData
    
    def getName(self):
        return self._name
    
    def getLayer(self):
        return self._layer
    
    def getAreaData(self):
        return self._areaData
        
    def getBandData(self):
        return self._bandData

