from qgis.core import QgsRasterLayer, QgsRaster

from osgeo import gdal 
import numpy as np
import math

from .base_classification import BaseClassification

class MinimalErrorClassification(BaseClassification):
    
    def __init__(self):
        BaseClassification.__init__(self)
    
    def generateClasses(self, rasterLayer, vectorPoints, roi_geometries, colors):
        return []
    
    def classifyLayer(self, layer, outputLayer, classes): 
        #First three items in list are the red, green and blue bands in output layer, 
        #followed by all bands in the input layer, ascending by band number 
        bandsArrays = []
        
        output_provider = outputLayer.dataProvider()
        output_data_source_uri = str(output_provider.dataSourceUri())
        output_raster = gdal.Open(str(output_provider.dataSourceUri()), gdal.GA_Update)
        for i in range(1,4):
            bandsArrays.append(output_raster.GetRasterBand(i).ReadAsArray())
        
        provider = layer.dataProvider()
        data_source_uri = str(provider.dataSourceUri())
        raster = gdal.Open(str(provider.dataSourceUri()), gdal.GA_Update) 
        for i in range(1,layer.bandCount()+1):
            bandsArrays.append(raster.GetRasterBand(i).ReadAsArray())
        
        self.progress.emit(0.2)
        pixelCount = bandsArrays[0].size
        nextTreshhold = int(pixelCount/100)
        progressCount = 0
        
        self.progress.emit(0.2)
        for it in np.nditer(bandsArrays, op_flags = ['readwrite']):
            progressCount += 1
            
            valuesDict = {}
            for i in range(3, len(it)):
                valuesDict[i-2] = it[i]
            selected_class = self._classifyPoint(valuesDict, self.classes)
            
            selected_color = selected_class.getColor()
            if selected_color is not None:
                it[0][...] = float(selected_color.red())
                it[1][...] = float(selected_color.green())
                it[2][...] = float(selected_color.blue())
            if progressCount > nextTreshhold:
                nextTreshhold += int(pixelCount/100)
                progress = progressCount / float(pixelCount)
                self.progress.emit((0.2 + progress) / 1.2)
        
        for i in range(1,4):
            output_raster.GetRasterBand(i).WriteArray(bandsArrays[i-1])
        
        raster = None
        provider = None
        output_raster = None
        output_provider = None
        
        self.olayer = QgsRasterLayer(output_data_source_uri)
        self.olayer.renderer().setRedBand(self.colorBandNumbers['RED'])
        self.olayer.renderer().setGreenBand(self.colorBandNumbers['GREEN'])
        self.olayer.renderer().setBlueBand(self.colorBandNumbers['BLUE'])
        
        self.correctLayerContrast(self.olayer)
        
        return self.olayer
    
    def _classifyPoint(self, bands_values, classes):
        if(len(classes) == 0):
            return None
        if(len(classes) == 1):
            return classes[0]
        selected_class = classes[0]
        best_err = classes[0].getError(bands_values)
        for i in range(1, len(classes)):
            new_err = classes[i].getError(bands_values)
            if (new_err < best_err):
                best_err = new_err
                selected_class = classes[i]
        return selected_class
        
        
class MinimalErrorClass:
    
    def __init__(self, color):
        self._color = color
    
    def getColor(self):
        return self._color
    
    def getError(self, valuesDict):
        return 0.0
        
    
    
        