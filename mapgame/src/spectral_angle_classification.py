from qgis.core import QgsRasterLayer, QgsRaster, QgsRasterBandStats

from osgeo import gdal 
import numpy as np
import math

from .minimal_error_classification import MinimalErrorClassification, MinimalErrorClass

class SpectralAngleClassification(MinimalErrorClassification):
    
    def __init__(self):
        MinimalErrorClassification.__init__(self)
    
    def generateClasses(self, rasterLayer, vectorPoints, roi_geometries, colors):
        bandMinMax = self._getBandMinMax(rasterLayer)
        classes = []
        for i in range(len(roi_geometries)):
            _class = self._getClass(vectorPoints, roi_geometries[i], rasterLayer, colors[i], bandMinMax)
            classes.append(_class)
        return classes
    
    def _getBandMinMax(self, rasterLayer):
        bandMinMax = {}
        for i in range(1, rasterLayer.bandCount() + 1):
            stats = rasterLayer.dataProvider().bandStatistics(i, QgsRasterBandStats.All, rasterLayer.extent(), 0)
            bandMinMax[i] = (stats.minimumValue, stats.maximumValue)
        print(bandMinMax)
        return bandMinMax
    
    def _getClass(self, points_list, roi, layer, color, bandMinMax):
        selected_points = []
        valuesDict = {}
        
        #Identify points in roi
        for feature in points_list:
            if feature.geometry().intersects(roi):
                selected_points.append(feature.geometry().asPoint())
        if (len(selected_points) == 0):
            return None
        
        #Get the sum of values in points for each band
        for point in selected_points:
            ident = layer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue)
            point_values = ident.results()
            for key in point_values.keys():
                val = point_values[key]
                if key in valuesDict:
                    valuesDict[key] += val
                else:
                    valuesDict[key] = val
        
        #Get average values
        for key in valuesDict.keys():
            valuesDict[key] = 1.0 * valuesDict[key] / len(selected_points)
        
        resultClass = SpectralAngleClass(valuesDict, color, bandMinMax)
        return resultClass
        
        
class SpectralAngleClass(MinimalErrorClass):
    
    def __init__(self, valuesDict, color, bandMinMax):
        MinimalErrorClass.__init__(self, color)
        self._bandMinMax = bandMinMax
        self._values = self.normalizeVector(valuesDict)
        print(self._values)
    
    def normalizeVector(self, vector):
        delKeys = []
        for key in vector.keys():
            val = vector[key]
            minVal = self._bandMinMax[key][0]
            maxVal = self._bandMinMax[key][1]
            if maxVal == minVal:
                vector[key] = 1.0
            else:
                bmin = 1.0
                bmax = maxVal - minVal + 1.0
                vector[key] = ( val - minVal ) * (bmax-bmin) / ( maxVal - minVal ) + bmin
        for key in delKeys:
            del vector[key]
        return vector
        
    def getError(self, valuesDict):
        vector1 = self.normalizeVector(valuesDict)
        vector2 = self._values
        dividend = 0.0
        divisorX = 0.0
        divisorY = 0.0
        for key in vector1.keys():
            dividend += float(vector1[key]) * vector2[key]     
            divisorX += float(vector1[key]) * vector1[key]
            divisorY += float(vector2[key]) * vector2[key]
        divisorX = math.sqrt(divisorX)
        divisorY = math.sqrt(divisorY)
        
        quotient = dividend / (divisorX * divisorY)
        
        if quotient >= 1.0:
            quotient = 0.9999999999999
        elif quotient <= -1.0:
            quotient = -0.9999999999999
        
        return math.acos(quotient)
        
    
    
        