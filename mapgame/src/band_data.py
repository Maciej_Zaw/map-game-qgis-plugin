from PyQt5.QtWidgets import QLabel, QWidget, QHBoxLayout, QVBoxLayout, QLineEdit, QListWidgetItem, QListWidget
from .model import BandData

class BandDataWidget(QWidget):

    def __init__(self, parent, bandNumber, bandName):
        QWidget.__init__(self, parent=parent)
        self._bandName = bandName
        self._bandNumber = bandNumber
        self.initUI()
        
    def initUI(self):
        self._layout = QHBoxLayout()
        self._label = QLabel(self)
        self._label.setText(self._bandName)
        
        self._textbox = QLineEdit(self)
        
        self._layout.setContentsMargins(2, 2, 2, 2)
        self._layout.addWidget(self._label)
        self._layout.addWidget(self._textbox)
        self.setLayout(self._layout)
        self.show()
    
    def getBandData(self):
        name = self._textbox.text()
        if len(name) == 0:
            name = self._bandName
        return BandData(self._bandNumber, name)
        
class BandDataMenuWidget(QWidget):
    
    def __init__(self, parent, layer):
        QWidget.__init__(self, parent=parent)
        self._layer = layer
        self._bandWidgets = []
        self.initUI()
    
    def initUI(self):
        self._layout = QVBoxLayout()    
        self._list = QListWidget(self)
        for i in range(1,self._layer.bandCount() + 1):
            newBand = BandDataWidget(self, i, self._layer.bandName(i))
            self.addBandWidget(newBand)
        self._layout.addWidget(self._list)
        
        self.setLayout(self._layout)
    
    def addBandWidget(self, bandWidget):
        self._bandWidgets.append(bandWidget)
        newListItem = QListWidgetItem(self._list)
        newListItem.setSizeHint(bandWidget.sizeHint())
        self._list.addItem(newListItem)
        self._list.setItemWidget(newListItem, bandWidget)
    
    def getBandDataArray(self):
        bands = []
        for band in self._bandWidgets:
            bands.append(band.getBandData())
        return bands