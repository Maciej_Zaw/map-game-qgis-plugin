from PyQt5.QtCore import QThread, Qt
from PyQt5.QtWidgets import QAction, QComboBox, QLabel, QDialog, QFileDialog, QPushButton, QWidget, QVBoxLayout, QProgressBar

from qgis.gui import QgsMapCanvas

from .area_selection import AreaSelectionMenuWidget
from .band_selection import BandSelectionMenuWidget
from .minimal_distance_classification import MinimalDistanceClassification
from .spectral_angle_classification import SpectralAngleClassification
from .bray_curtis_classification import BrayCurtisClassification
from .main_canvas import MainCanvas

class MainGameDialog(QDialog):

    def __init__(self, gameData, parent=None):
        super(MainGameDialog, self).__init__(parent)
        self._title = gameData.getName()
        self._layer = gameData.getLayer()
        self._bandData = gameData.getBandData()
        self._areaData = gameData.getAreaData()
        self._mainCanvas = MainCanvas(self._layer)
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(self._title)
        self._layout = QVBoxLayout()
        
        self._bandMenu = BandSelectionMenuWidget(self, self._bandData, self._layer)       
        self._areasMenu = AreaSelectionMenuWidget(self, self._mainCanvas, self._areaData)
        
        self._algorithmSelection = QComboBox(self)
        self._algorithmSelection.addItem("Minimal Distance", "minimal_distance")
        self._algorithmSelection.addItem("Spectral Angle", "spectral_angle")
        self._algorithmSelection.addItem("Bray Curtis Similarity", "bray_curtis")
        
        self._classifyButton = QPushButton(self)
        self._classifyButton.setText("Classify")
        self._classifyButton.clicked.connect(self.runClassification)
        
        self._progressBar = QProgressBar(self)
        self._progressBar.setMinimum(0)
        self._progressBar.setMaximum(100)
        
        self._layout.addWidget(self._bandMenu)
        self._layout.addWidget(self._areasMenu)
        self._layout.addWidget(self._algorithmSelection)
        self._layout.addWidget(self._classifyButton)
        self._layout.addWidget(self._progressBar)
        
        self.setLayout(self._layout)
        self.move(50,200)
        
    def _getClassificationObject(self):
        algorithmName = self._algorithmSelection.currentData()
        if algorithmName == "minimal_distance":
            return MinimalDistanceClassification()
        if algorithmName == "spectral_angle":
            return SpectralAngleClassification()
        if algorithmName == "bray_curtis":
            return BrayCurtisClassification()
        return None
        
        
    def runClassification(self):
        classificationObject = self._getClassificationObject()       
        classificationObject.setArguments(self._layer, self._areasMenu.getAreas(), self._areaData)
        classificationThread = QThread(self)      
        classificationObject.moveToThread(classificationThread)
        classificationObject.finished.connect(self.onClassificationCompleted)
        classificationObject.progress.connect(self.setProgressBarValue)
        classificationThread.started.connect(classificationObject.run)
        classificationThread.start()
        
        self.classificationThread = classificationThread
        self.classificationObject = classificationObject
    
    def setProgressBarValue(self, fraction):
        val = int(100 * fraction)
        if val < 0:
            val = 0
        if val > 100:
            val = 100
        self._progressBar.setValue(val)
    
    def onClassificationCompleted(self, result):
        if result is None:
            return
        self.olayer = result['outputLayer']
        self.ocanvas = QgsMapCanvas()
        self.ocanvas.setExtent(self.olayer.extent())
        self.ocanvas.setLayers([self.olayer])
        self.ocanvas.show()
        self.ocanvas.setGeometry(501,200,1080,720)
        
        if result['totalPoints'] > 0:
            self.resultDialog = GameResultDialog(result['correctPoints'], result['totalPoints'], self.olayer)
        else:
            self.resultDialog = GameResultDialog(0, 0, self.olayer)
        
    def closeEvent(self, event):
        self._mainCanvas.destroy()
        self.reject()

        
class GameResultDialog(QDialog):

    def __init__(self, correct, total, layer, parent=None):
        QDialog.__init__(self, parent=parent)
        self._correct = correct
        self._total = total
        self._layer = layer
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle("Congratulations!")
        self.resize(300,150)
        self._layout = QVBoxLayout(self)
        
        self.label = QLabel(self)
        if (self._total > 0):
            score = 100.0 * self._correct / self._total
            resultText = "Final score: %.2f" % score
            self.label.setAlignment(Qt.AlignCenter);
            self.label.setText(resultText)
        
        self._saveButton = QPushButton()
        self._saveButton.setText("Save map as image")
        self._saveButton.clicked.connect(self.saveResult)
        
        self._layout.addWidget(self.label)
        self._layout.addWidget(self._saveButton)
        self.setLayout(self._layout)
        
        self.show()
    
    def saveResult(self):
        file = QFileDialog.getSaveFileName(self, 'Select file', None,"Images (*.png)")
        if file is not None:
            path = file[0]
            self.saveAsImage(path)
    
    def saveAsImage(self, path):
        self._path = path
        self.canvas = QgsMapCanvas()
        self.canvas.setExtent(self._layer.extent())
        self.canvas.setLayers([self._layer]) 
        self.canvas.mapCanvasRefreshed.connect( self.saveImageCallback )
        self.canvas.refresh()
        
    
    def saveImageCallback(self):
        self.canvas.saveAsImage(self._path)