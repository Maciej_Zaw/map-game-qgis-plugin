from PyQt5.QtCore import Qt
from qgis.core import QgsPoint, QgsPointXY, QgsRaster
from qgis.gui import QgsMapToolEmitPoint, QgsMapToolPan, QgsMapCanvas

class MainCanvas:

    def __init__(self, layer):
        self._layer = layer
        self._canvas = QgsMapCanvas()
        self._canvas.setExtent(self._layer.extent())
        self._canvas.setLayers([self._layer])
        self._canvas.setWindowFlag(Qt.WindowCloseButtonHint, False);
        self._canvas.show()
        self._canvas.setGeometry(401,200,1080,720)
        self._selectionAreas = None
        self._selectedIndex = None
        self._selectedArea = None
        
        self.tool_clickPoint = QgsMapToolEmitPoint(self._canvas)
        self.tool_clickPoint.canvasClicked.connect(self._clicked)
        self.tool_pan = QgsMapToolPan(self._canvas)
        
        self._setPanTool()
    
    def resetExtent(self):
        self._canvas.setExtent(self._layer.extent())
        self._layer.triggerRepaint()
    
    def _setEmitPointTool(self):
        self._canvas.setMapTool(self.tool_clickPoint)
    
    def _setPanTool(self):
        self._canvas.setMapTool(self.tool_pan)
    
    def getCanvas(self):
        return self._canvas
    
    def show(self):
        self._canvas.show()
    
    def hide(self):
        self._canvas.hide()
    
    def destroy(self):
        self._canvas.destroy()
    
    def setSelectionAreas(self, areasArray):
        self._selectionAreas = areasArray
    
    def clearSelectionArea(self):
        self._selectedArea = None
        self._setPanTool()
    
    def getCanvas(self):
        return self._canvas
    
    def setSelectedArea(self, area):
        self._selectedArea = area
        self._setEmitPointTool()
    
    def _clicked(self, point, button):
        val, res = self._layer.dataProvider().sample(QgsPointXY(point), 1)
        
        point_values = self._layer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue).results()

        if self._selectedArea is None:
            return
        self._selectedArea.addPoint(QgsPoint(point))
        self._selectedArea.drawArea()
        

