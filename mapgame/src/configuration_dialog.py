from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QDialog, QPushButton, QWidget, QHBoxLayout, QVBoxLayout, QLineEdit, QColorDialog

from qgis.gui import QgsMapCanvas

from .area_selection import AreaSelectionMenuWidget
from .model import AreaData, BandData, GameData
from .band_data import BandDataMenuWidget
from .main_canvas import MainCanvas

class ConfigurationDialog(QDialog):

    def __init__(self, parent, layer):
        super(ConfigurationDialog, self).__init__(parent)
        self._layer = layer
        
        self.initUI()
        

    def initUI(self):    
        self.setWindowTitle("Configuration")
        self._mainCanvas = MainCanvas(self._layer)
        
        self._layout = QVBoxLayout()
        
        self._nameInput = QLineEdit(self)
        self._nameInput.textChanged.connect(self.onChanged)
        
        self._bandMenu = BandDataMenuWidget(self, self._layer)
        
        self._areasMenu = AreaSelectionMenuWidget(self, self._mainCanvas, [], isEditable=True)
        self._areasMenu.removed.connect(self.onChanged)
        self._bandDataWidget = NewClassWidget(self, self._areasMenu)
        self._bandDataWidget.added.connect(self.onChanged)
        
        self._acceptButton = QPushButton(self)
        self._acceptButton.setText("Accept")
        self._acceptButton.clicked.connect(self.onAccept)
        self._acceptButton.setEnabled(False)
        
        self._layout.addWidget(self._nameInput)
        self._layout.addWidget(self._bandMenu)
        self._layout.addWidget(self._areasMenu)
        self._layout.addWidget(self._bandDataWidget)
        self._layout.addWidget(self._acceptButton)
        
        self.setLayout(self._layout)
        self.move(50,200)
    
    def getGameData(self):
        gameName = self._nameInput.text()
        areaData = self._areasMenu.getCurrentAreasData()
        bandData = self._bandMenu.getBandDataArray()
        gameData = GameData(gameName, self._layer, areaData, bandData)
        return gameData
    
    def _isGameDataValid(self):
        if len(self._nameInput.text().strip()) == 0:
            return False
        if len(self._areasMenu.getCurrentAreasData()) < 2:
            return False
        return True
    
    def onAccept(self):
        self._mainCanvas.destroy()
        self.accept()
    
    def onChanged(self):
        self._acceptButton.setEnabled(self._isGameDataValid())
        
    def closeEvent(self, event):
        self._mainCanvas.destroy()
        self.reject()
        

class NewClassWidget(QWidget):
    added = pyqtSignal(object)
    
    def __init__(self, parent, areasMenu):
        QWidget.__init__(self, parent=parent)
        self._color = QColor(0,0,0,125)
        self._areasMenu = areasMenu
        self._areasMenu.removed.connect(self.onChanged)
        self.initUI()
    
    def initUI(self):
        self._layout = QHBoxLayout()
        
        self._colorButton = QPushButton(self)
        self.updateColor()
        self._colorButton.clicked.connect(self.selectColor)
        
        self._textbox = QLineEdit(self)
        self._textbox.textChanged.connect(self.onChanged)
        
        self._addButton = QPushButton(self)
        self._addButton.setText("+")
        self._addButton.setEnabled(False)
        self._addButton.clicked.connect(self.addClass)
        
        self._layout.setContentsMargins(2, 2, 2, 2)
        self._layout.addWidget(self._colorButton)
        self._layout.addWidget(self._textbox)
        self._layout.addWidget(self._addButton)
        self.setLayout(self._layout)
        self.show()
        
    def onChanged(self):
        if len(self._textbox.text().strip()) == 0:
            self._addButton.setEnabled(False)
            return
        area = AreaData(self._textbox.text(), self._color)
        unique = self._areasMenu.isUnique(area)
        self._addButton.setEnabled(unique)
    
    def selectColor(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self._color = color
            self._color.setAlpha(125)
            self.updateColor()
        self.onChanged()
            
    
    def addClass(self):
        if len(self._textbox.text()) == 0:
            return
        area = AreaData(self._textbox.text(), self._color)
        self._areasMenu.addArea(area)
        self.added.emit(area)
        self._addButton.setEnabled(False)
        
    
    def updateColor(self):
        style = "background-color:rgb(" + str(self._color.red()) + ',' + str(self._color.green()) + ',' + str(self._color.blue()) + ')'
        self._colorButton.setStyleSheet(style)
    