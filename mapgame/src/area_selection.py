from qgis.core import QgsGeometry, QgsPointXY
from qgis.gui import QgsRubberBand, QgsVertexMarker, QgsMapCanvas
from PyQt5.QtGui import QPolygonF, QPixmap
from PyQt5.QtCore import pyqtSignal, QSize
from PyQt5.QtWidgets import QPushButton, QLabel, QHBoxLayout, QVBoxLayout, QWidget, QListWidget, QListWidgetItem

from .model import AreaData

class AreaSelectionWidget(QWidget):
    
    def __init__(self, parent, main_canvas, areaData, onDeleteCallback=None):
        QWidget.__init__(self, parent=parent)
        self._mainCanvas = main_canvas
        self.canvas = main_canvas.getCanvas()
        self._areaData = areaData
        self.points = []
        self.rubberBand = None
        self.geometry = None
        self.markers = []
        self.parent = parent
        self._onDeleteCallback = onDeleteCallback
        self.initUI()
        
    def initUI(self):
        self._layout = QHBoxLayout()
        self._layout.setContentsMargins(2, 2, 2, 2)
        
        color = self._areaData.getColor()
        self.colorCube = QWidget()
        self.colorCube.setFixedSize(QSize(20,20))
        style = "background-color:rgb(" + str(color.red()) + ',' 
        style += str(color.green()) + ',' 
        style += str(color.blue()) + ');'
        style += " border: solid;"
        self.colorCube.setStyleSheet(style)
        self._layout.addWidget(self.colorCube)
        
        self.label = QLabel(self.parent)
        self.label.setText(self._areaData.getName())
        self._layout.addWidget(self.label)
        
        self.clearButton = QPushButton(self.parent)
        self.clearButton.setText("Clear")
        self.clearButton.clicked.connect(self.onClearButtonPush)      
        self._layout.addWidget(self.clearButton)
        
        self.deleteButton = None
        if self._onDeleteCallback is not None:
            self.deleteButton = QPushButton(self.parent)
            self.deleteButton.setFixedSize(QSize(20,20))
            self.deleteButton.setText("x")
            self.deleteButton.clicked.connect(self.onDelete)      
            self._layout.addWidget(self.deleteButton)
        
        self.setLayout(self._layout)
        self.show()
    
    def select(self):
        self.drawArea()
        self._mainCanvas.setSelectedArea(self)
    
    def getId(self):
        return self._areaData.getId()
    
    def getAreaData(self):
        newArea = AreaData(self._areaData.getName(), self._areaData.getColor(), points=self.getPointsXY())
        return newArea
    
    def onClearButtonPush(self):
        self.clearArea()
        self.points = []
    
    def onDelete(self):
        self._onDeleteCallback(self)
        
    def addPoint(self,point):
        self.points.append(point)  
    
    def getPointsXY(self):
        pointsXY = []
        for point in self.points:
            pointsXY.append(QgsPointXY(point))
        return pointsXY  
        
    def drawArea(self):
        self.clearArea()
        drawnPoints = []
        self.markers = []
        for point in self.points:
            drawnPoints.append(point)
            m = QgsVertexMarker(self.canvas)
            self.markers.append(m)
            m.setCenter(QgsPointXY(point))
            m.setColor(self._areaData.getColor())
        if len(self.points) > 1:
            closed = len(self.points) > 2
            self.rubberBand = QgsRubberBand(self.canvas, closed)
            if closed:
                drawnPoints.append(self.points[0])
                self.geometry = self.createPolygon(drawnPoints)
            else:
                self.geometry = QgsGeometry.fromPolyline(drawnPoints)
            self.rubberBand.setToGeometry(self.geometry, None)
            self.rubberBand.setWidth(2)
            self.rubberBand.setColor(self._areaData.getColor())
    
    def clearArea(self):
        if self.rubberBand is not None:
            self.canvas.scene().removeItem(self.rubberBand)
            self.rubberBand = None
        if len(self.markers) != 0:
            for marker in self.markers:
                self.canvas.scene().removeItem(marker)
        self.markers = []
    
    def createPolygon(self, points):
        list_polygon = QPolygonF()
        for point in points:
            list_polygon.append(point.toQPointF())
        return QgsGeometry.fromQPolygonF(list_polygon)
    
    def getSelectedGeometry(self):
        return self.geometry
    
    def getColor(self):
        return self._areaData.getColor()


class AreaSelectionMenuWidget(QWidget):
    removed = pyqtSignal(object)
    
    def __init__(self, parent, mainCanvas, areasData, isEditable=False):
        QWidget.__init__(self, parent=parent)
        self._areaItemTuples = []
        self._canvas = mainCanvas
        self._areasData = areasData
        self._isEditable = isEditable
        self.initUI()
    
    def initUI(self):
        self._layout = QVBoxLayout()
        self._list = QListWidget(self)
        self._list.itemSelectionChanged.connect(self._onItemSelectionChanged)
        
        for area in self._areasData:
            self.addArea(area)
        
        self._layout.addWidget(self._list)
        
        self.clearButton = QPushButton(self)
        self.clearButton.setText("Clear selection")
        self.clearButton.clicked.connect(self.clearSelection)      
        self._layout.addWidget(self.clearButton)
        
        self.setLayout(self._layout)
    
    def clearSelection(self):
        item = self._list.currentItem()
        if item is not None:
            item.setSelected(False)
        self._canvas.clearSelectionArea()
        
    def getAreas(self):
        areas = []
        for tuple in self._areaItemTuples:
            areas.append(tuple[0])
        return areas
    
    def addArea(self, area):
        if self._isEditable:
            newArea = AreaSelectionWidget(self, self._canvas, area, onDeleteCallback=self.removeWidget)
        else:
            newArea = AreaSelectionWidget(self, self._canvas, area)
        self._addAreaToList(newArea)
    
    def _addAreaToList(self, area):
        newListItem = QListWidgetItem(self._list)
        newListItem.setSizeHint(area.sizeHint())
        self._list.addItem(newListItem)
        self._list.setItemWidget(newListItem, area)
        self._areaItemTuples.append((area, newListItem))
    
    def _getWidgetByItem(self, item):
        for tuple in self._areaItemTuples:
            if tuple[1] == item:
                return tuple[0]
        return None
    
    def _getItemByWidget(self, widget):
        for tuple in self._areaItemTuples:
            if tuple[0] == widget:
                return tuple[1]
        return None
    
    def _onItemSelectionChanged(self):
        item = self._list.currentItem()
        area = self._list.itemWidget(item)
        area.select()
    
    def removeWidget(self, area):
        for tuple in self._areaItemTuples:
            if tuple[0] == area:
                area.clearArea()
                self._list.takeItem(self._list.row(tuple[1]))
                self._areaItemTuples.remove(tuple)
                self.removed.emit(area)
    
    def getCurrentAreasData(self):
        areas = []
        for tuple in self._areaItemTuples:
            area = tuple[0]
            areas.append(area.getAreaData())
        return areas
    
    def isUnique(self, area):
        for tuple in self._areaItemTuples:
            areaSelection = tuple[0]
            if areaSelection.getAreaData().isConflicting(area):
                return False
        return True
        
    