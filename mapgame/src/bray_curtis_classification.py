from qgis.core import QgsRasterLayer, QgsRaster, QgsRasterBandStats

from osgeo import gdal 
import numpy as np
import math

from .minimal_error_classification import MinimalErrorClassification, MinimalErrorClass

class BrayCurtisClassification(MinimalErrorClassification):
    
    def __init__(self):
        MinimalErrorClassification.__init__(self)
    
    def generateClasses(self, rasterLayer, vectorPoints, roi_geometries, colors):
        classes = []
        for i in range(len(roi_geometries)):
            _class = self._getClass(vectorPoints, roi_geometries[i], rasterLayer, colors[i])
            classes.append(_class)
        return classes
    
    def _getClass(self, points_list, roi, layer, color):
        selected_points = []
        valuesDict = {}
        
        #Identify points in roi
        for feature in points_list:
            if feature.geometry().intersects(roi):
                selected_points.append(feature.geometry().asPoint())
        if (len(selected_points) == 0):
            return None
        
        #Get the sum of values in points for each band
        for point in selected_points:
            ident = layer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue)
            point_values = ident.results()
            for key in point_values.keys():
                val = point_values[key]
                if key in valuesDict:
                    valuesDict[key] += val
                else:
                    valuesDict[key] = val
        
        #Get average values
        for key in valuesDict.keys():
            valuesDict[key] = 1.0 * valuesDict[key] / len(selected_points)
        
        resultClass = BrayCurtisClass(valuesDict, color)
        return resultClass
        
        
class BrayCurtisClass(MinimalErrorClass):
    
    def __init__(self, valuesDict, color):
        MinimalErrorClass.__init__(self, color)
        self._values = valuesDict
        print(self._values)
        
    def getError(self, valuesDict):
        vector1 = valuesDict
        vector2 = self._values
        dividend = 0.0
        divisorX = 0.0
        divisorY = 0.0
        for key in vector1.keys():
            dividend += abs(float(vector1[key]) - vector2[key])     
            divisorX += float(vector1[key])
            divisorY += float(vector2[key])
        divisorX = math.sqrt(divisorX)
        divisorY = math.sqrt(divisorY)
        
        if divisorX + divisorY == 0.0:
            if divisorX == 0.0:
                return 0.0
            else:
                return 1.0
        return dividend / (divisorX + divisorY)
        
    
    
        