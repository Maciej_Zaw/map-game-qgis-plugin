from PyQt5.QtCore import QObject, pyqtSignal

from qgis.core import QgsRasterLayer, QgsRaster, QgsContrastEnhancement, QgsGeometry
from qgis.gui import QgsMapToolEmitPoint
import processing

from osgeo import gdal 
import random

class BaseClassification(QObject):
    
    finished = pyqtSignal(object)
    error = pyqtSignal(basestring)
    progress = pyqtSignal(float)
    
    def __init__(self):
        QObject.__init__(self)
        self.colorBandNumbers = {
            'RED': 1,
            'GREEN': 2,
            'BLUE': 3
        }
        self.result = None
        self.kill = False
        
    def setArguments(self, rasterLayer, areas, areaData):
        self.arguments = {
            'rasterLayer': rasterLayer,
            'outputLayer': self._cloneLayer(rasterLayer),
            'feat_points': self._generateVectorPoints(rasterLayer),
            'areas': areas,
            'areaData': areaData
        }
        
    def run(self):
        self.result = None
        try:
            self.progress.emit(0.0)
            outputLayer = self.runClassification(self.arguments['rasterLayer'], self.arguments['outputLayer'], self.arguments['feat_points'], self.arguments['areas'])
            correctFeaturePoints, totalFeaturePoints = self._getScore(outputLayer, self.arguments['feat_points'], self.arguments['areaData'] )
            self.result = {
                'correctPoints': correctFeaturePoints,
                'totalPoints': totalFeaturePoints,
                'outputLayer': outputLayer
            }
            self.progress.emit(1.0)
        except e:
            self.error.emit("ERROR")
        self.finished.emit(self.result)
         
    
    def getResult(self):
        return self.result
    
    def kill(self):
        self._killed = True
        
    def runClassification(self, rasterLayer, outputLayer, feat_points, areas):
        roi_geometries = []
        colors = []
        
        for i in range(len(areas)):
            geom = areas[i].getSelectedGeometry()
            if (geom is not None):
                roi_geometries.append(geom)
                colors.append(areas[i].getColor())
            
        self.classes = self.generateClasses(rasterLayer, feat_points, roi_geometries, colors)
        
        return self.classifyLayer(rasterLayer, outputLayer, self.classes)
    
    def generateClasses(self, rasterLayer, vectorPoints, roi_geometries, colors):
        return []
    
    def classifyLayer(self, layer, outputLayer, classes): 
        self.olayer = outputLayer
        return self.olayer
    
    #set the renderer to use 0-255 range for all color values
    def correctLayerContrast(self, layer):
        band = layer.renderer().redBand()
        type = layer.renderer().dataType(band)
        enhancement = self._getBandEnhancement(band, type)
        layer.renderer().setRedContrastEnhancement(enhancement)
        
        band = layer.renderer().greenBand()
        type = layer.renderer().dataType(band)
        enhancement = self._getBandEnhancement(band, type)
        layer.renderer().setGreenContrastEnhancement(enhancement)
        
        band = layer.renderer().blueBand()
        type = layer.renderer().dataType(band)
        enhancement = self._getBandEnhancement(band, type)
        layer.renderer().setBlueContrastEnhancement(enhancement)
        
        layer.triggerRepaint()
    
    def _getBandEnhancement(self, band, dataType):
        enhancement = QgsContrastEnhancement(dataType)
        enhancement.setContrastEnhancementAlgorithm(QgsContrastEnhancement.StretchToMinimumMaximum, True)
        enhancement.setMinimumValue(0)
        enhancement.setMaximumValue(255)
        return enhancement
    
    def _cloneLayer(self, layer):
        seed = str(random.randint(0,999999))
        output_buffer_dir = 'memory:merged__mdc_output_raster_buffer'+seed+'.tif' 
        
        res = processing.run("gdal:merge", {'INPUT': [layer],
              'PCT':False,
              'SEPARATE':True,  
              'NODATA_INPUT' : None, 
              'NODATA_OUTPUT' : None,
              'DATA_TYPE': 5,
              'OPTIONS' : '',
              'OUTPUT': output_buffer_dir})
        
        return QgsRasterLayer(res['OUTPUT'])
    
    def _generateVectorPoints(self, layer):
        seed = str(random.randint(0,999999))
        points_buffer_dir = 'memory:pointsFromRaster'+seed+'.tif'    
        res = processing.run("native:pixelstopoints", {'INPUT_RASTER': layer,
              'RASTER_BAND':1,
              'FIELD_NAME':'Value',  
              'OUTPUT': points_buffer_dir})        
        vl = res['OUTPUT']
        
        return list(vl.getFeatures())
        
    def _getScore(self, layer, points_list, areaData):
        totalPoints = 0
        correctPoints = 0
        
        areasWithPointsArray = []
        for area in areaData:
            if len(area.getPoints()) > 2:
                gPolygon = QgsGeometry.fromPolygonXY([area.getPoints()])
                areasWithPointsArray.append({
                    'color': area.getColor(),
                    'points': [],
                    'geometry': gPolygon
                })
            
        
        for feature in points_list:
            for areaWithPoints in areasWithPointsArray:
                if feature.geometry().intersects(areaWithPoints['geometry']):
                    areaWithPoints['points'].append(feature.geometry().asPoint())
        
        for areaWithPoints in areasWithPointsArray:
            correctPointsForArea, totalPointsForArea = self._getScoreForPoints(layer, areaWithPoints['points'], areaWithPoints['color'])
            correctPoints += correctPointsForArea
            totalPoints += totalPointsForArea
        
        return correctPoints, totalPoints
    
    def _getScoreForPoints(self, layer, feat_points, correctColor):
        totalPoints = 0
        correctPoints = 0
        for point in feat_points:
            totalPoints += 1
            ident = layer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue)
            point_values = ident.results()
            incorrectColor = False
            for key in point_values.keys():
                if key == self.colorBandNumbers['RED']:
                    if point_values[key] != correctColor.red():
                        incorrectColor = True
                        break
                elif key == self.colorBandNumbers['GREEN']:
                    if point_values[key] != correctColor.green():
                        incorrectColor = True
                        break
                elif key == self.colorBandNumbers['BLUE']:
                    if point_values[key] != correctColor.blue():
                        incorrectColor = True
                        break
            if not incorrectColor:
                correctPoints += 1
        return correctPoints, totalPoints
        
    
    
        