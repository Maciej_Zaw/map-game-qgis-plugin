from qgis.core import QgsRasterLayer, QgsRaster

from osgeo import gdal 
import numpy as np
import math

from .minimal_error_classification import MinimalErrorClassification, MinimalErrorClass

class MinimalDistanceClassification(MinimalErrorClassification):
    
    def __init__(self):
        MinimalErrorClassification.__init__(self)
    
    def generateClasses(self, rasterLayer, vectorPoints, roi_geometries, colors):
        classes = []
        for i in range(len(roi_geometries)):
            _class = self._getClass(vectorPoints, roi_geometries[i], rasterLayer, colors[i])
            classes.append(_class)
        return classes
    
    def _getClass(self, points_list, roi, layer, color):
        selected_points = []
        valuesDict = {}
        
        #Identify points in roi
        for feature in points_list:
            if feature.geometry().intersects(roi):
                selected_points.append(feature.geometry().asPoint())
        if (len(selected_points) == 0):
            return None
        
        #Get the sum of values in points for each band
        for point in selected_points:
            ident = layer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue)
            point_values = ident.results()
            for key in point_values.keys():
                if key in valuesDict:
                    valuesDict[key]+=point_values[key]
                else:
                    valuesDict[key]=point_values[key]
        
        #Get average values
        for key in valuesDict.keys():
            valuesDict[key] = 1.0 * valuesDict[key] / len(selected_points)
        
        resultClass = MinimalDistanceClass(valuesDict, color)
        return resultClass
        
        
class MinimalDistanceClass(MinimalErrorClass):
    
    def __init__(self, valuesDict, color):
        MinimalErrorClass.__init__(self, color)
        self._values = valuesDict
    
    def getError(self, valuesDict):
        distance = 0.0
        for key in valuesDict.keys():
            diff = valuesDict[key] - self._values[key]
            distance += diff * diff       
        return math.sqrt(distance)
        
    
    
        